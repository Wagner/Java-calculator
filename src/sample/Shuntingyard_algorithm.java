package sample;

import java.util.*;
class OutputQueue extends Stack{
    Map<String, Runnable> operations = new HashMap<String, Runnable>(){{
        put("+", () -> sum());
        put("-", () -> subtract());
        put("*", () -> multiply());
        put("/", () -> divide());
        put("^", () -> pow());
        put("%%", () -> mod());
    }};
    private void sum(){
        double right = (Double) pop();
        double left = (Double) pop();
        push(left + right);
    }

    void subtract(){
        double right = (Double) pop();
        double left = (Double) pop();
        push(left-right);
    }
    void multiply(){
        double right = (Double) pop();
        double left = (Double) pop();
        push(left*right);
    }
    void divide(){

        double right = (Double) pop();
        double left = (Double) pop();
        push(left/right);
    }
    void pow(){
        double right = (Double) pop();
        double left = (Double) pop();
        push(Math.pow(left, right));
    }
    void mod(){
        double right = (Double) pop();
        double left = (Double) pop();
        push(left%right);
    }
}

public class Shuntingyard_algorithm {
    private StringBuilder buffer;
    private static final List<String> exceptions = Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "0",
            "*", "/", "+", "-", "^", "%", "(", ")", ".");
    //private static final List<String> operands = Arrays.asList("+", "-", "/", "*", "^", "%");
    private OutputQueue output = new  OutputQueue();
    private static final Map<String, Integer> operands_map = new HashMap<String, Integer>(){{
        put("^", 4);
        //put("**", 4);
        put("%%", 3);
        put("*", 3);
        put("/", 3);
        put("+", 2);
        put("-", 2);
        put("(", -99);
    }};


    private boolean is_double(String string){
        try{
            Double.valueOf(string);
            return true;
        }
        catch (NumberFormatException e){
            return false;
        }
    }
    public String read_string(String input){
        buffer = new StringBuilder(input);
        convert_to_polish();
        return calculate();
    }


    private void convert_to_polish(){
        StringBuilder operator_builder = new StringBuilder();
        StringBuilder number_builder = new StringBuilder();
        Stack<String> operator_stack = new Stack<>();
        String token;
        //buffer.append((double) 0);
        System.out.println(buffer);
        while (buffer.length() > 0){

            token = buffer.substring(0,1);
//            System.out.println(token);
        if (!exceptions.contains(token)){
            System.out.println("Exception: Dziwny znak wykryto \"" + token + "\" znak został pominięty.");
        }
        else {
//             System.out.println(output);
            if (is_double(token) || token.equals(".")){
                number_builder.append(token);
            }
            else if(number_builder.length()>0){
                output.add(Double.valueOf(number_builder.toString()));
                number_builder.setLength(0);
            }
            if (token.contains("%")) token = "%%";
            if (operands_map.keySet().contains(token)){
                operator_builder.append(token);
            }
            else if (operator_builder.length()>0){
                if (!(operator_stack.isEmpty())) {
                    do{
                        if ((operands_map.get(operator_stack.peek()) >= operands_map.get(operator_builder.toString())) ) //&& !operator_stack.peek().equals("^")
                            output.add(operator_stack.pop());
                        else
                            break;
                    }
                    while (!(operator_stack.isEmpty()));
                }
                operator_stack.add(operator_builder.toString());
                operator_builder.setLength(0);
            }
            if (token.equals("(")){
                operator_stack.add("(");
            }
            else if (token.equals(")")){
                while(!(operator_stack.peek().equals("("))){
                    output.add(operator_stack.pop());
                }
                operator_stack.pop();
            }
        }
        buffer.deleteCharAt(0);
        }
        if (!(number_builder.length() == 0))
        output.add(Double.valueOf(number_builder.toString()));
        while (operator_stack.size() != 0){
            output.add(operator_stack.pop());
        }
//        System.out.println(output);
        System.out.println(operator_stack);
    }
    private String calculate(){
        Collections.reverse(output);
        OutputQueue computational_stack = new OutputQueue();
        while (output.size()>0){
            if (output.peek() instanceof Double)
                computational_stack.push(output.pop());
            else
                computational_stack.operations.get(output.pop().toString()).run();
//            System.out.println(output);
//            System.out.println(computational_stack);
    }
    return computational_stack.pop().toString();
    }
}


