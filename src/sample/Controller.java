package sample;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.event.EventHandler;
import java.net.URL;
import java.sql.Time;
import java.util.ResourceBundle;

public class Controller implements Initializable{
    public Button summation, zero, one, two, three, four, five, six, seven, eight, nine, dot, subtract, mod, multiply, divide, open, close, eq, power;
    public TextField display;
    private KeyEvent t;
    public Shuntingyard_algorithm alg = new Shuntingyard_algorithm();
    public void initialize(URL url, ResourceBundle rb) {
        one.setOnAction(e -> handleButtonAction("1"));
        two.setOnAction(e -> handleButtonAction("2"));
        three.setOnAction(e -> handleButtonAction("3"));
        four.setOnAction(e -> handleButtonAction("4"));
        five.setOnAction(e -> handleButtonAction("5"));
        six.setOnAction(e -> handleButtonAction("6"));
        seven.setOnAction(e -> handleButtonAction("7"));
        eight.setOnAction(e -> handleButtonAction("8"));
        nine.setOnAction(e -> handleButtonAction("9"));
        zero.setOnAction(e -> handleButtonAction("0"));
        summation.setOnAction(e -> handleButtonAction("+"));
        subtract.setOnAction(e -> handleButtonAction("-"));
        mod.setOnAction(e -> handleButtonAction("%"));
        multiply.setOnAction(e -> handleButtonAction("*"));
        divide.setOnAction(e -> handleButtonAction("/"));
        open.setOnAction(e -> handleButtonAction("("));
        close.setOnAction(e -> handleButtonAction(")"));
        power.setOnAction(e -> handleButtonAction("^"));
        eq.setOnAction(e -> run_equation());
        display.setOnKeyPressed(new EventHandler<KeyEvent>() {
            public void handle(KeyEvent ke) {
                if (ke.getCode() == KeyCode.ENTER)
                    run_equation();
            }
        });
    }
    private void handleButtonAction(String str) {
        display.setText(display.getText() + str);
    }
    private void run_equation(){
        String result = alg.read_string(display.getText());

//        System.out.println(result);
        display.setText(result);
    }

}

