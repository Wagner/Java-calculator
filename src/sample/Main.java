package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Shuntingyard_algorithm alg = new Shuntingyard_algorithm();
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Kalkulator");
        Scene scena = new Scene(root, 300, 200);
        primaryStage.setScene(scena);
        primaryStage.show();

    }



    public static void main(String[] args) {
        launch(args);
    }
}
